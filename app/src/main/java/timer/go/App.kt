package timer.go

import android.app.Application
import com.chibatching.kotpref.Kotpref
import timber.log.Timber

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Kotpref.init(applicationContext)
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}