package timer.go.ui.main

import android.os.SystemClock

data class TimerState(
    val isActive: Boolean = true,
    val isRunning: Boolean = false,
    val remainingTime: Long = 0L,
    val remainingPeriods: Int = 0,
    val mainTimeEnded: Boolean = false,
    val won: Boolean = false
)

val TimerState.isOver
    get() = remainingTime <= 0 && remainingPeriods <= 0

class PeriodTimer(
    private var remainingTime: Long,
    private val periodTime: Long,
    private var remainingPeriods: Int
) : Timer {
    override var isActive: Boolean = true
    override var isRunning: Boolean = false
    private var tickTime: Long = 0
    private var currentPeriodTime: Long = periodTime

    private val mainTimeEnded: Boolean
        get() = remainingTime <= 0

    override fun pause(resetPeriod: Boolean) {
        isActive = false
        isRunning = false
        if (resetPeriod && remainingTime <= 0 && currentPeriodTime <= periodTime && remainingPeriods > 0) {
            currentPeriodTime = periodTime
        }
    }

    override fun resume() {
        tickTime = SystemClock.elapsedRealtime()
        isActive = true
        isRunning = true
    }

    override fun tick(): TimerState {
        if (!isRunning) {
            val time = if (mainTimeEnded) currentPeriodTime else remainingTime
            return TimerState(isActive, isRunning, time, remainingPeriods, mainTimeEnded)
        }
        val currentTime = SystemClock.elapsedRealtime()
        val diff = currentTime - tickTime
        tickTime = currentTime
        val updatedRemainingTime = remainingTime - diff
        if (updatedRemainingTime > 0) {
            remainingTime = updatedRemainingTime
            return TimerState(isActive, isRunning, remainingTime, remainingPeriods, mainTimeEnded)
        }
        remainingTime = 0
        val updatedPeriodTime = currentPeriodTime - diff
        if (updatedPeriodTime > 0) {
            currentPeriodTime = updatedPeriodTime
        } else {
            currentPeriodTime = 0
            if (remainingPeriods > 0) {
                currentPeriodTime = periodTime
                remainingPeriods--
            }
        }
        return TimerState(isActive, isRunning, currentPeriodTime, remainingPeriods, mainTimeEnded)
    }
}