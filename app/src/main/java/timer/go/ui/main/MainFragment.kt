package timer.go.ui.main

import android.os.Bundle
import android.view.View
import androidx.core.view.isInvisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_main.*
import timer.go.R
import timer.go.announce.GameAnnouncer


class MainFragment : Fragment(R.layout.fragment_main) {

    private val viewModel: TimerViewModel by activityViewModels()

    private lateinit var announcer: GameAnnouncer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        announcer = GameAnnouncer(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setPlayerButtons()
        setControlButtons()
        viewModel.gameState.observe(viewLifecycleOwner) { state ->
            pauseButton.isInvisible = state !is GameState.Active
            when (state) {
                is GameState.Active -> {
                    pauseButton.setImageResource(R.drawable.ic_pause_24px)
                    pauseButton.contentDescription = getString(R.string.pause_description)
                }
                is GameState.Paused -> {
                    pauseButton.setImageResource(R.drawable.ic_play_arrow_24px)
                    pauseButton.contentDescription = getString(R.string.play_description)
                }
            }
        }
        viewModel.player1State.observe(viewLifecycleOwner) { state ->
            player1Button.timerState = state
            announcer.announceState(isFirst = true, timerState = state)
        }
        viewModel.player2State.observe(viewLifecycleOwner) { state ->
            player2Button.timerState = state
            announcer.announceState(isFirst = false, timerState = state)
        }
    }

    override fun onPause() {
        super.onPause()
        viewModel.pause()
    }

    private fun setControlButtons() {
        settingsButton.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_settingsFragment)
        }
        pauseButton.setOnClickListener {
            viewModel.pause()
        }
        restartButton.setOnClickListener {
            viewModel.restart()
        }
    }

    private fun setPlayerButtons() {
        player1Button.setOnClickListener {
            viewModel.player1Click()
        }
        player2Button.setOnClickListener {
            viewModel.player2Click()
        }
    }
}
