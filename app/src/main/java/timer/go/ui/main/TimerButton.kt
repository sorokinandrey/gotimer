package timer.go.ui.main

import android.content.Context
import android.media.MediaPlayer
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.view_timer_button.view.*
import timer.go.R
import timer.go.toTimeString

class TimerButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val clickSound = MediaPlayer.create(context, R.raw.sound_button_click)

    init {
        inflate(context, R.layout.view_timer_button, this)
        setBackgroundResource(R.drawable.bg_timer_button)
    }

    var timerState: TimerState? = null
        set(value) {
            field = value
            value ?: return
            isEnabled = value.isActive
            if (value.remainingTime == 0L && value.remainingPeriods == 0) {
                period.isVisible = false
                val endText = if (value.won) {
                    R.string.won
                } else {
                    R.string.lost
                }
                time.setText(endText)
                return
            }
            period.text = value.remainingPeriods.toString()
            time.text = value.remainingTime.toTimeString()
        }

    override fun setOnClickListener(clickListener: OnClickListener?) {
        super.setOnClickListener {
            if (clickSound.isPlaying) {
                clickSound.stop()
            }
            clickSound.start()
            clickListener?.onClick(it)
        }
    }
}