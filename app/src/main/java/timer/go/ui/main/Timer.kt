package timer.go.ui.main

interface Timer {
    val isActive: Boolean

    fun tick(): TimerState

    fun pause(resetPeriod: Boolean = true)

    fun resume()
    var isRunning: Boolean
}