package timer.go.ui.main

import android.os.SystemClock
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timer.go.Dependencies


sealed class GameState {
    object NotStarted : GameState()
    data class Active(val isFirstTurn: Boolean) : GameState()
    data class Paused(val isFirstTurn: Boolean) : GameState()
    data class Ended(val isFirstWon: Boolean) : GameState()
}

private const val TIMER_STEP = 100L

class TimerViewModel : ViewModel() {
    private val prefs = Dependencies.prefs
    private var gameJob: Job? = null
    private var startTime: Long = 0

    private val _gameState = MutableLiveData<GameState>()
    val gameState: LiveData<GameState>
        get() = _gameState

    private val _player1State = MutableLiveData<TimerState>()
    val player1State: LiveData<TimerState>
        get() = _player1State

    private val _player2State = MutableLiveData<TimerState>()
    val player2State: LiveData<TimerState>
        get() = _player2State

    private lateinit var timer1: Timer
    private lateinit var timer2: Timer

    private fun defaultTimer() = PeriodTimer(prefs.mainTime, prefs.periodTime, prefs.periods)

    init {
        restart()
    }

    private fun startGame() {
        startTime = SystemClock.elapsedRealtime()
        gameJob?.cancel()
        gameJob = viewModelScope.launch {
            while (true) {
                when (val state = _gameState.value) {
                    is GameState.Active -> {
                        val (timer, playerState) = state.timerAndState
                        val nextTimerState = timer.tick()
                        if (nextTimerState.isOver) {
                            val firstWon = !state.isFirstTurn
                            _gameState.value = GameState.Ended(firstWon)
                            _player1State.value =
                                TimerState(isActive = false, isRunning = false, won = firstWon)
                            _player2State.value =
                                TimerState(isActive = false, isRunning = false, won = !firstWon)
                        } else {
                            playerState.value = nextTimerState
                        }
                    }
                    else -> Unit
                }
                delay(TIMER_STEP)
            }
        }
        return
    }

    private inline val GameState.Active.timerAndState: Pair<Timer, MutableLiveData<TimerState>>
        get() = if (isFirstTurn) timer1 to _player1State else timer2 to _player2State

    private inline val GameState.Paused.timerAndState: Pair<Timer, MutableLiveData<TimerState>>
        get() = if (isFirstTurn) timer1 to _player1State else timer2 to _player2State

    fun player1Click() {
        if (_gameState.value is GameState.NotStarted) {
            startGame()
        } else {
            timer1.pause()
        }
        _gameState.value = GameState.Active(isFirstTurn = false)
        timer2.resume()
        timer1.pause()
        _player1State.value = timer1.tick()
    }

    fun player2Click() {
        if (_gameState.value is GameState.NotStarted) {
            startGame()
        }
        _gameState.value = GameState.Active(isFirstTurn = true)
        timer1.resume()
        timer2.pause()
        _player2State.value = timer2.tick()
    }


    fun restart() {
        timer1 = defaultTimer()
        timer2 = defaultTimer()
        _player1State.value = timer1.tick()
        _player2State.value = timer2.tick()
        _gameState.value = GameState.NotStarted
    }

    private fun <T> MutableLiveData<T>.setIfNew(newValue: T) {
        if (newValue != value) {
            value = newValue
        }
    }

    fun pause() {
        when (val state = gameState.value) {
            is GameState.Active -> {
                _gameState.value = GameState.Paused(state.isFirstTurn)
                val (timer, _) = state.timerAndState
                timer.pause(resetPeriod = false)
            }
            is GameState.Paused -> {
                _gameState.value = GameState.Active(state.isFirstTurn)
                val (timer, _) = state.timerAndState
                timer.resume()
            }
            else -> Unit
        }
    }
}
