package timer.go.ui.settings

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.toolbar.*
import timber.log.Timber
import timer.go.Dependencies
import timer.go.MainActivity
import timer.go.R
import timer.go.toArrayAdapter
import timer.go.ui.main.TimerViewModel

class SettingsFragment : Fragment(R.layout.fragment_settings) {
    private val prefs = Dependencies.prefs
    private val viewModel: TimerViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val activity = activity as MainActivity
        activity.setSupportActionBar(toolbar)
        setupActionBarWithNavController(activity, findNavController())
        with(timerTypeSelector) {
            val timers = listOf("Byo-Yomi")
            setAdapter(timers.toArrayAdapter(requireContext()))
            setText(prefs.timerType, false)
            onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
                val selectedType = timers[position]
                Timber.d("Selected %s", selectedType)
                prefs.timerType = selectedType
                viewModel.restart()
            }
        }
        with(mainTimeSelector) {
            val times = (0..50 step 10).map { it.secondsToMillis() } +
                    listOf(1, 5, 10).map { it.minutesToMillis() }
            val selectedIndex = times.indexOf(prefs.mainTime)
            val timeLabels = times.map { it.toTimeLabel() }
            setAdapter(timeLabels.toArrayAdapter(requireContext()))
            setText(timeLabels[selectedIndex], false)
            onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
                val selectedTime = times[position]
                Timber.d("Selected %d", selectedTime)
                prefs.mainTime = selectedTime
                viewModel.restart()
            }
        }
        with(periodSelector) {
            val periods = (0..5).toList()
            val periodLabels = periods.map { it.toString() }
            val selectedIndex = periods.indexOf(prefs.periods)
            setAdapter(periodLabels.toArrayAdapter(requireContext()))
            setText(periodLabels[selectedIndex], false)
            onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
                val selectedPeriodNumber = periods[position]
                Timber.d("Selected %d", selectedPeriodNumber)
                prefs.periods = selectedPeriodNumber
                viewModel.restart()
            }
        }
        with(periodTimeSelector) {
            val periodTimes = (10..60 step 10).map { it.secondsToMillis() }
            val periodLabels = periodTimes.map { it.toTimeLabel() }
            val selectedIndex = periodTimes.indexOf(prefs.periodTime)
            setAdapter(periodLabels.toArrayAdapter(requireContext()))
            setText(periodLabels[selectedIndex], false)
            onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
                val selectedTime = periodTimes[position]
                Timber.d("Selected %d", selectedTime)
                prefs.periodTime = selectedTime
                viewModel.restart()
            }
        }
    }

    private fun Int.secondsToMillis() = toLong() * 1000
    private fun Int.minutesToMillis() = toLong() * 1000 * 60

    private fun Long.toTimeLabel(): CharSequence {
        val minutes = (this / 1000 / 60).toInt()
        val seconds = (this / 1000).toInt()
        return if (minutes > 0) {
            resources.getQuantityString(R.plurals.minute, minutes, minutes)
        } else {
            resources.getQuantityString(R.plurals.second, seconds, seconds)
        }
    }
}