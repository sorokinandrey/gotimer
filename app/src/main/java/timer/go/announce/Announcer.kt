package timer.go.announce

import android.content.Context
import android.speech.tts.TextToSpeech
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import timber.log.Timber


private const val TTS_ID = "GoTimerAnnouncer"

@UseExperimental(ExperimentalCoroutinesApi::class, FlowPreview::class)
open class Announcer(context: Context) {
    private val announceChannel = Channel<AnnounceCommand>(Channel.BUFFERED)
    private val resources = context.resources
    private val scope = CoroutineScope(Dispatchers.Default)

    private val ttsInitListener: TextToSpeech.OnInitListener =
        TextToSpeech.OnInitListener { result ->
            if (result != TextToSpeech.SUCCESS) {
                Timber.e("Couldn't initialize TTS %d", result)
                return@OnInitListener
            }
            tts.setSpeechRate(1.2f)
            announceChannel.consumeAsFlow()
                .onEach { command ->
                    val text = when (command) {
                        is AnnounceString -> resources.getString(command.stringRes)
                        is AnnounceFormatString -> resources.getString(
                            command.stringRes,
                            *command.formatArgs
                        )
                    }
                    tts.speak(text, TextToSpeech.QUEUE_ADD, null, TTS_ID)
                }
                .launchIn(scope)
        }

    private val tts = TextToSpeech(context, ttsInitListener, "com.google.android.tts")

    fun announce(command: AnnounceCommand) {
        announceChannel.offer(command)
    }
}