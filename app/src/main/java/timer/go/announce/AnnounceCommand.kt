package timer.go.announce

sealed class AnnounceCommand
data class AnnounceString(val stringRes: Int) : AnnounceCommand()
class AnnounceFormatString(val stringRes: Int, vararg val formatArgs: Any) : AnnounceCommand() {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AnnounceFormatString

        if (stringRes != other.stringRes) return false
        if (!formatArgs.contentEquals(other.formatArgs)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = stringRes
        result = 31 * result + formatArgs.contentHashCode()
        return result
    }
}