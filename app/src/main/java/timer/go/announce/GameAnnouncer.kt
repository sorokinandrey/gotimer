package timer.go.announce

import android.content.Context
import timer.go.R
import timer.go.ui.main.TimerState

class GameAnnouncer(context: Context) : Announcer(context) {
    private var previousStates = arrayOfNulls<TimerState>(2)

    fun announceState(isFirst: Boolean, timerState: TimerState) {
        val previousState = previousStates[if (isFirst) 0 else 1]
        if (timerState.isRunning) {
            if (timerState.mainTimeEnded && timerState.remainingPeriods != previousState?.remainingPeriods) {
                val command = if (timerState.remainingPeriods == 0) {
                    AnnounceString(R.string.last_period_remains)
                } else {
                    AnnounceFormatString(R.string.periods_remains, timerState.remainingPeriods)
                }
                announce(command)
            } else {
                val secondsRemains = timerState.secondsRemains()
                if (secondsRemains != previousState?.secondsRemains() && secondsRemains in 0..10) {
                    announce(
                        AnnounceFormatString(
                            R.string.number,
                            secondsRemains
                        )
                    )
                }
            }
        }
        previousStates[if (isFirst) 0 else 1] = timerState
    }
}

private fun TimerState.secondsRemains(): Int {
    return if (remainingPeriods == 0) {
        (remainingTime / 1000).toInt() % 60
    } else -1
}