package timer.go

import com.chibatching.kotpref.KotprefModel

class Prefs : KotprefModel() {
    var timerType: String by stringPref("Byo-Yomi")
    var mainTime: Long by longPref(1 * 60 * 1000)
    var periods: Int by intPref(4)
    var periodTime: Long by longPref(30 * 1000)
}