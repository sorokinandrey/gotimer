package timer.go

import android.R
import android.content.Context
import android.widget.ArrayAdapter
import android.widget.ListAdapter
import androidx.annotation.LayoutRes

fun Long.toTimeString(): String {
    val minutes = (this / 1000) / 60
    return if (minutes > 0) {
        String.format(
            "%2d:%02d.%1d",
            minutes,
            (this / 1000) % 60,
            (this % 1000) / 100
        )
    } else {
        String.format(
            "%2d.%1d",
            (this / 1000) % 60,
            (this % 1000) / 100
        )
    }
}

fun <T> Iterable<T>.toArrayAdapter(context: Context, @LayoutRes itemLayout: Int = R.layout.simple_dropdown_item_1line): ArrayAdapter<T> {
    return ArrayAdapter(
        context,
        itemLayout,
        this.toList()
    )
}
